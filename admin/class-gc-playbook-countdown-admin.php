<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Playbook_Countdown
 * @subpackage Gc_Playbook_Countdown/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gc_Playbook_Countdown
 * @subpackage Gc_Playbook_Countdown/admin
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gc_Playbook_Countdown_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Gc_Playbook_Countdown_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Gc_Playbook_Countdown_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/gc-playbook-countdown-admin.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Gc_Playbook_Countdown_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Gc_Playbook_Countdown_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/gc-playbook-countdown-admin.js', array('jquery'), $this->version, false);

    }

    /**
     * Add plugin settings menu entry in the backend.
     *
     * @since    1.0.0
     */
    public function gcpc_add_admin_menu()
    {

        add_menu_page(
            'Playbook Countdown', // $page_title
            'Playbook Countdown', // $menu_title
            'manage_options', // $capability
            $this->plugin_name, // $menu_slug
            array($this, 'gcpc_options_page'), // $render_page_function
            'dashicons-book' // $icon
            );

    }


    /**
     * Plugin settings page.
     *
     * @since    1.0.0
     */
    public function gcpc_options_page()
    {

        include(plugin_dir_path(__FILE__) . 'partials/' . $this->plugin_name . '-admin-display.php');

    }


    /**
     * Initialize plugin settings fields.
     *
     * @since    1.0.0
     */
    public function gcpc_settings_init()
    {

        register_setting(
            $this->plugin_name . '-options',
            $this->plugin_name . '-options'
            );

        add_settings_section(
            $this->plugin_name . '-section',
            __('Specify quantity of books available and interval to count them down', $this->plugin_name),
            array($this, 'gcpc_settings_section_callback'),
            $this->plugin_name
            );

        add_settings_field(
            'gcpc_timer_interval',
            __('Books countdown interval in seconds', $this->plugin_name),
            array($this, 'gcpc_text_field_render'),
            $this->plugin_name,
            $this->plugin_name . '-section',
            array(
                'id' => 'gcpc_timer_interval'
                )
            );

        add_settings_field(
            'gcpc_timer_countdown',
            __('Books quantity', $this->plugin_name),
            array($this, 'gcpc_text_field_render'),
            $this->plugin_name,
            $this->plugin_name . '-section',
            array(
                'id' => 'gcpc_timer_countdown'
                )
            );

    }


    /**
     * Render input field for each option (interval & countdown).
     *
     * @since    1.0.0
     */
    public function gcpc_text_field_render($args)
    {

        $options = get_option($this->plugin_name . '-options');
        // print_r($options);
        include(plugin_dir_path(__FILE__) . 'partials/' . $this->plugin_name . '-field-text-admin-display.php');

    }


    public function gcpc_settings_section_callback()
    {

        // _e( 'This section description', $this->plugin_name );

    }


    /**
     * Schedule the cron after updating options.
     *
     * @since    1.0.0
     */
    public function gcpc_schedule_cron() {

        if ( ! wp_next_scheduled( 'gcpc_countdown_event' ) ) {
            wp_schedule_event( time(), 'gcpc_custom_timer_interval', 'gcpc_countdown_event' );
        }

    }


    public function gcpc_schedule_cron_recurrence( $schedules ) {

        $options = get_option($this->plugin_name . '-options');

        $schedules['gcpc_custom_timer_interval'] = array(
            'display' => __( 'Customized by timer_interval option', $this->plugin_name ),
            'interval' => (int)$options['gcpc_timer_interval'],
            );
        return $schedules;

    }


    public function gcpc_do_decrement() {

        $options = get_option($this->plugin_name . '-options');

        if ((int)$options['gcpc_timer_countdown'] > 0) {
            $options['gcpc_timer_countdown'] = (int)$options['gcpc_timer_countdown'] - 1;
            update_option($this->plugin_name . '-options', $options);
        }
    }


}
