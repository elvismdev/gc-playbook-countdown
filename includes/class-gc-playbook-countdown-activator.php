<?php

/**
 * Fired during plugin activation
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Playbook_Countdown
 * @subpackage Gc_Playbook_Countdown/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gc_Playbook_Countdown
 * @subpackage Gc_Playbook_Countdown/includes
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gc_Playbook_Countdown_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
