<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://elvismdev.io/
 * @since      1.0.0
 *
 * @package    Gc_Playbook_Countdown
 * @subpackage Gc_Playbook_Countdown/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gc_Playbook_Countdown
 * @subpackage Gc_Playbook_Countdown/includes
 * @author     Elvis Morales <elvis@grantcardone.com>
 */
class Gc_Playbook_Countdown_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		wp_clear_scheduled_hook('gcpc_custom_timer_interval');
	}

}
