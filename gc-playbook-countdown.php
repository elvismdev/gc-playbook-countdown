<?php

/**
 *
 * @link              http://elvismdev.io/
 * @since             1.0.0
 * @package           Gc_Playbook_Countdown
 *
 * @wordpress-plugin
 * Plugin Name:       GC Playbook Countdown
 * Plugin URI:        https://bitbucket.org/grantcardone/gc-playbook-countdown
 * Description:       Automated book countdown for PlayBook To Millions.
 * Version:           1.0.0
 * Author:            Elvis Morales
 * Author URI:        http://elvismdev.io/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gc-playbook-countdown
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gc-playbook-countdown-activator.php
 */
function activate_gc_playbook_countdown() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gc-playbook-countdown-activator.php';
	Gc_Playbook_Countdown_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gc-playbook-countdown-deactivator.php
 */
function deactivate_gc_playbook_countdown() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gc-playbook-countdown-deactivator.php';
	Gc_Playbook_Countdown_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gc_playbook_countdown' );
register_deactivation_hook( __FILE__, 'deactivate_gc_playbook_countdown' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gc-playbook-countdown.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gc_playbook_countdown() {

	$plugin = new Gc_Playbook_Countdown();
	$plugin->run();

}
run_gc_playbook_countdown();
